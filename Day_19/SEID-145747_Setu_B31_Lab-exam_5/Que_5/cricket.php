<?php


class cricket
{
    function __construct()
    {
        echo "This constructor is from cricket <br/> ";
    }
}

class movie extends cricket{
    function __construct()
    {
        echo "This constructor is from movie <br/> ";
        parent:: __construct();
    }
}

$construct = new movie();

