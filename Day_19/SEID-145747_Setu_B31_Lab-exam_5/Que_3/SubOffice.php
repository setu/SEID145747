<?php


class SubOffice
{
    protected $Branch1 = 'Mirpur';
    protected $Branch2 = 'Farmgate';

    protected function set()
    {
        echo "This is our $this->Branch1 branch <br/>";

    }

    protected function get()
    {
        echo "This is our $this->Branch2 branch";

    }

}

class MainOffice extends SubOffice
{
    public function info()
    {
        $this->set();
        $this->get();
    }
}

$branch = new MainOffice();
$branch->info();