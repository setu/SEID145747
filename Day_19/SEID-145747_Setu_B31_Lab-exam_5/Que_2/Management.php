<?php


class Management
{
    public $Name = 'Tonu Islam';
    public $Department = 'Web App Development';

    public function setName()
    {
        echo " This is $this->Name .<br/> ";
    }

    public function getName()
    {
        echo "She is a team member of $this->Department team.";
    }

}

class StudentManagement extends Management
{


}

$stdMan = new StudentManagement();
$stdMan->setName();
$stdMan->getName();

