<?php
include_once("vendor/autoload.php");

use App\SEIP\ID_145747\Module\About\About;

$obj = new About("BITM");

use App\SEIP\ID_145747\Module\Contact\Contact;
$Obj4con = new Contact('+880123456789');

use App\SEIP\ID_145747\Module\Image\Image;
$obj4im = new Image('Profile Picture');

use App\SEIP\ID_145747\Module\Profile\Profile;
$obj4pro = new Profile ('My profile');

use App\SEIP\ID_145747\Module\Users\Users;
$obj4User = new Users('Setu Islam');

