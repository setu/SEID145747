<?php
$x = "NULL";
echo "Result of is_null example 1 : ";
echo is_null($x);
echo "<br/>";
echo "<br/>";


$x = " ";
echo "Result of is_null example 2 : ";
if (is_null($x)) {
    echo "This is a null variable";
} else {
    echo "This is not a null variable";
}