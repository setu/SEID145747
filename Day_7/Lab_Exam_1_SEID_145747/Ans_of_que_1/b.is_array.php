<?php
$x = array("Hello!", "this", "is", "first", "lab exam");
echo "Result of is_array example 1 : ";
echo is_array($x);
echo "</br>";
echo "</br>";


$y = array("Hello!", "this", "is", "first", "lab exam");
echo "Result of is_array example 2 : ";
if (is_array($y)) {
    echo "This is an array";
} else {
    echo "This is not an array";
}