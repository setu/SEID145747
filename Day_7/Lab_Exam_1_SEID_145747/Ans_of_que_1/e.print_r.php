<?php
$x = array("Hello!", "this", "is", "first", "lab exam");
echo "Result of print_r example 1 : ";
echo "<pre>";
print_r($x);
echo "</br>";
echo "</br>";


$y = array("Hello!", "this", "is", "first", "lab exam");
echo "Result of print_r example 2 : ";
if (is_array($y)) {
    echo "<pre>";
    print_r($y);
} else {
    echo "This is not an array";
}