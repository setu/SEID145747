<?php

abstract class Institute

{
    public abstract function JNU();
    public abstract function DU();
}

class BUET extends Institute{
    public function JNU(){

        echo " JNU is simply dummy text of the printing and typesetting industry. <br/>";
    }
    public function DU(){
        echo "DU is simply dummy text of the printing and typesetting industry.";

    }
}

$obj= new BUET();
$obj->JNU();
$obj->DU();

