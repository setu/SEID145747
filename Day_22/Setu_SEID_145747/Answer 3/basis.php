<?php
interface bitm {
    public function abc();
    public function test();
}
interface basis {
    public function php31();
    public function webapp();

}
interface pondit {
    public function xyz();
    public function mytest();

}
interface seip{
    public function day22();
    public function exam_6();

}

class abtinterface implements bitm,basis,pondit,seip{
    public function abc(){
        echo "Contrary to popular belief, Lorem Ipsum is not simply random text.
         It has roots in a piece of classical Latin literature from 45 BC <br/>";

    }
    public function test()
    {
        echo "Contrary to popular belief, Lorem Ipsum is not simply random text.
         It has roots in a piece of classical Latin literature from 45 BC <br/>";
    }
    public function php31()
    {
        echo "Contrary to popular belief, Lorem Ipsum is not simply random text.
         It has roots in a piece of classical Latin literature from 45 BC <br/>";
    }
    public function webapp()
    {
        echo "Contrary to popular belief, Lorem Ipsum is not simply random text.
         It has roots in a piece of classical Latin literature from 45 BC <br/>";
    }
    public function xyz()
    {
        echo "Contrary to popular belief, Lorem Ipsum is not simply random text.
         It has roots in a piece of classical Latin literature from 45 BC <br/>";
    }
    public function mytest()
    {
        echo "Contrary to popular belief, Lorem Ipsum is not simply random text.
         It has roots in a piece of classical Latin literature from 45 BC <br/>";
    }
    public function day22()
    {
        echo "Contrary to popular belief, Lorem Ipsum is not simply random text.
         It has roots in a piece of classical Latin literature from 45 BC <br/>";
    }
    public function exam_6()
    {
        echo "Contrary to popular belief, Lorem Ipsum is not simply random text.
         It has roots in a piece of classical Latin literature from 45 BC <br/>";
    }
}

$obj = new abtinterface();
$obj->abc();
$obj->test();
$obj->php31();
$obj->webapp();
$obj->xyz();
$obj->mytest();
$obj->day22();
$obj->exam_6();

